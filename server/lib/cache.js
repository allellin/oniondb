/**
 * Created by Fizz on 21/4/15.
 */
var DB=require("./db");
var helper=require("./helper");

function Cache(){

    if(!(this instanceof  Cache)) return new Cache();

    this._collections=[];

}
module.exports=new Cache();

Cache.prototype.writeSnapShot=function(doc,dbName){
    var collection=this._collections[dbName]=(this._collections[dbName] || []);
    collection.push(doc);
};

Cache.prototype.insert=function(value,dbName){
    var timestamp=(new Date()).valueOf();
    var doc={
        timestamp:timestamp,
        value:value
    };
    this.writeSnapShot(doc,dbName);
    return doc;
};

Cache.prototype.update=function(selector,value){

};

Cache.prototype.remove=function(selector,value){

};

Cache.prototype.query=function(startTimestamp,endTimestamp,dbName,callback){
    console.log(arguments)
    if(!callback)   return;
    var queryInDB=[],
        queryInCache=[],
        startIndex=-1,
        endIndex;
    var collection=this._collections[dbName]=(this._collections[dbName] || []);
    var length=collection.length;
    if(length>0) {
        startIndex = helper.binarySearch(collection, -1, length, startTimestamp);
        endIndex = helper.binarySearch(collection, startIndex - 1, length, endTimestamp);
        queryInCache = collection.slice(Math.ceil(startIndex), Math.floor(endIndex) + 1);
    }
    if(startIndex>=0){
        //nothing in db
            callback(queryInCache);
    }else{
        //some data in db
        DB.query(startTimestamp,endTimestamp,dbName,function(docs){
            queryInDB=docs;
            callback(queryInDB.concat(queryInCache));
        });
    }
};

var _syncDatabase=function(){
    console.log("batching");
    var _collections=module.exports._collections;
    console.log(_collections);
    for(var name in _collections){
        var collection=_collections[name];
        var length=collection.length;
        if(length>0){
            DB.batch(collection,name);
        }
    }
    module.exports._collections=[];
    setTimeout(_syncDatabase,3000);
};
_syncDatabase();



