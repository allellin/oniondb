/**
 * Created by Fizz on 20/4/15.
 */

//config
MONGO_URL="mongodb://localhost:27017/test";

//cache
COLLECTION_LIMIT=5;

//sql state
ONION_INSERT = 10001;
ONION_UPDATE = 10002;
ONION_REMOVE = 10003;
ONION_MAX=10004;
ONION_MIN=10005;
ONION_AVERAGE=10006;

//onion init
ONION_INIT=20001;