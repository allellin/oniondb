var constant=require("./constant");

var MongoClient=require("mongodb").MongoClient;
var assert=require("assert");

function DB(){
    if(!(this instanceof DB)) return new DB();
}

module.exports=new DB();

DB.prototype.batch=function(docs,dbName){
    MongoClient.connect(MONGO_URL,function(err,db){
        assert.equal(null,err);
        var collection=db.collection(dbName);
        collection.insertMany(docs);
        db.close();
    });
};

DB.prototype.query=function(startTimestamp,endTimestamp,dbName,callback){

    if(!callback){
        return;
    }
    MongoClient.connect(MONGO_URL,function(err,db){
        assert.equal(null,err);
        var collection=db.collection(dbName);
        var result=collection.find({
            timestamp:{
                $gte:startTimestamp,
                $lte:endTimestamp
            }
        }).toArray(function(err,docs){
            callback(docs);
        });

    });
};