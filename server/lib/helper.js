var helper={};

module.exports=helper;

helper.binarySearch=function(collection,low,high,timestamp){
    if((low+1)>=high){
        return (low+0.5)
    }
    var middle=parseInt((low+high)/2);
    if(timestamp<collection[middle].timestamp){
        return helper.binarySearch(collection,low,middle,timestamp);
    }else if(timestamp>collection[middle].timestamp){
        return helper.binarySearch(collection,middle,high,timestamp);
    }else if(collection[middle].timestamp===timestamp){
        return middle;
    }
};

helper.max=function(docs,fn){
    var max=null;
    for(var i in docs){
        if(max){
            var number=parseFloat(fn.call(null,docs[i].value));
            if(number>max){
                max=number
            }
        }else{
            max=parseFloat(fn.call(null,docs[i].value));
        }
    }
    return max;
};

helper.min=function(docs,fn){
    var min=null;
    for(var i in docs){
        if(min){
            var number=parseFloat(fn.call(null,docs[i].value));
            if(number<min){
                min=number
            }
        }else{
            min=parseFloat(fn.call(null,docs[i].value));
        }
    }
    return min;
};

helper.average=function(docs,fn){
    var length=docs.length;
    if(length===0){
        return null;
    }
    var total=0.00;
    for(var i in docs){
        total+=(parseFloat(fn.call(null,docs[i].value))||0.00);
    }
    return total/length;
};