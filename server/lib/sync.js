/**
 * Created by Fizz on 21/4/15.
 */
var webSocketServer = require('websocket').server;
var Cache = require("./cache");
var helper = require("./helper");

var ConnManager = function () {

    var _wsConnections = [];

    this.pushConnection = function (dbName, connection) {
        connection.dbName = dbName;
        _wsConnections[dbName] = _wsConnections[dbName] || [];
        _wsConnections[dbName].push(connection);
    };

    this.sendMessage = function (dbName, message) {
        var connections = _wsConnections[dbName] || [];
        message = (message instanceof Object) ? (JSON.stringify(message)) : message;
        for (var i in connections) {
            if (connections[i].connected)
                connections[i].sendUTF(message);
        }
    };

    this.sendMessageToOne=function(connection,message){
        message = (message instanceof Object) ? (JSON.stringify(message)) : message;
        connection.sendUTF(message);
    }
};

module.exports = function (httpServer) {

    var ws = new webSocketServer({
        httpServer: httpServer
    });

    var connManager = new ConnManager();

    ws.on("request", function (request) {

        var connection = request.accept();
        connection.on('message', function (message) {
            var message = JSON.parse((message.type === 'utf8') ? message.utf8Data : message.binaryData || null);
            var queryType = parseInt(message.queryType || null);
            var value = message.value || null;
            var selector = message.selector || null;
            if (queryType) {
                var responseMsg = {
                    queryType: queryType,
                    doc: null
                };
                if (queryType === ONION_INSERT) {
                    responseMsg.doc = Cache.insert(value, connection.dbName)
                } else if (queryType === ONION_UPDATE) {
                    Cache.update(selector, value)
                } else if (queryType === ONION_REMOVE) {
                    Cache.remove(selector, value)
                } else if (queryType === ONION_INIT) {
                    var dbName = message.dbName;
                    connManager.pushConnection(dbName, connection)
                } else{
                    var fn = selector.fn || function (value) {
                        return value;
                    };
                    var startTimestamp = selector.startTimestamp ? parseInt(selector.startTimestamp) : 0;
                    var endTimestamp = selector.endTimestamp ? parseInt(selector.endTimestamp) : (new Date()).valueOf();
                    Cache.query(startTimestamp, endTimestamp, connection.dbName, function (docs) {
                        if (queryType === ONION_MAX){
                            responseMsg.doc=helper.max(docs, fn);
                        }else if(queryType===ONION_MIN){
                            responseMsg.doc=helper.min(docs, fn);
                        }else if(queryType===ONION_AVERAGE){
                            responseMsg.doc=helper.average(docs, fn);
                        }
                        console.log(responseMsg);
                        connManager.sendMessageToOne(connection,responseMsg)
                    });

                    //if not return, message will be sent to all clients
                    return;
                }

                connManager.sendMessage(connection.dbName, responseMsg);
            }
        });
//        var obj={
//            queryType:ONION_INSERT
//        };
//        connection.sendUTF(JSON.stringify(obj));
//        setInterval(function() {
//            connection.sendUTF(JSON.stringify(obj));
//        },5000)
    });

    return ws;
};